#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Se debe ejecutar como root"
  exit
fi

apt update && sudo apt upgrade
apt install curl

curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

mkdir $HOME/pandorafms; cd $HOME/pandorafms
wget http://pfms.me/centos8-docker-compose
docker-compose -f centos8-docker-compose up